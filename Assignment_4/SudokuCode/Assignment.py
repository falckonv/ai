import copy
import itertools
# ADDED
import numpy as np


class CSP:
    def __init__(self):
        # self.variables is a list of the variable names in the CSP
        self.variables = []

        # self.domains[i] is a list of legal values for variable i
        self.domains = {}

        # self.constraints[i][j] is a list of legal value pairs for
        # the variable pair (i, j)
        self.constraints = {}

    def add_variable(self, name, domain):
        """Add a new variable to the CSP. 'name' is the variable name
        and 'domain' is a list of the legal values for the variable.
        """
        self.variables.append(name)
        self.domains[name] = list(domain)
        self.constraints[name] = {}

    def get_all_possible_pairs(self, a, b):
        """Get a list of all possible pairs (as tuples) of the values in
        the lists 'a' and 'b', where the first component comes from list
        'a' and the second component comes from list 'b'.
        """
        return itertools.product(a, b)

    def get_all_arcs(self):
        """Get a list of all arcs/constraints that have been defined in
        the CSP. The arcs/constraints are represented as tuples (i, j),
        indicating a constraint between variable 'i' and 'j'.
        """
        return [(i, j) for i in self.constraints for j in self.constraints[i]]

    def get_all_neighboring_arcs(self, var):
        """Get a list of all arcs/constraints going to/euefrom variable
        'var'. The arcs/constraints are represented as in get_all_arcs().
        """
        return [(i, var) for i in self.constraints[var]]

    def add_constraint_one_way(self, i, j, filter_function):
        """Add a new constraint between variables 'i' and 'j'. The legal
        values are specified by supplying a function 'filter_function',
        that returns True for legal value pairs and False for illegal
        value pairs. This function only adds the constraint one way,
        from i -> j. You must ensure that the function also gets called
        to add the constraint the other way, j -> i, as all constraints
        are supposed to be two-way connections!
        """
        if not j in self.constraints[i]:
            # First, get a list of all possible pairs of values between variables i and j
            self.constraints[i][j] = self.get_all_possible_pairs(self.domains[i], self.domains[j])

        # Next, filter this list of value pairs through the function
        # 'filter_function', so that only the legal value pairs remain
        self.constraints[i][j] = list(filter(lambda value_pair: filter_function(*value_pair), self.constraints[i][j]))

    def add_all_different_constraint(self, variables):
        """Add an Alldiff constraint between all of the variables in the
        list 'variables'.
        """
        for (i, j) in self.get_all_possible_pairs(variables, variables):
            if i != j:
                self.add_constraint_one_way(i, j, lambda x, y: x != y)

    def backtracking_search(self):
        """This functions starts the CSP solver and returns the found
        solution.
        """
        # Make a so-called "deep copy" of the dictionary containing the
        # domains of the CSP variables. The deep copy is required to
        # ensure that any changes made to 'assignment' does not have any
        # side effects elsewhere.
        assignment = copy.deepcopy(self.domains)

        # Run AC-3 on all constraints in the CSP, to weed out all of the
        # values that are not arc-consistent to begin with
        
        #self.inference(assignment, self.get_all_arcs())
        
        
        # Call backtrack with the partial assignment 'assignment'
        return self.backtrack(assignment)

    def backtrack(self, assignment):
        """The function 'Backtrack' from the pseudocode in the
        textbook.

        The function is called recursively, with a partial assignment of
        values 'assignment'. 'assignment' is a dictionary that contains
        a list of all legal values for the variables that have *not* yet
        been decided, and a list of only a single value for the
        variables that *have* been decided.

        When all of the variables in 'assignment' have lists of length
        one, i.e. when all variables have been assigned a value, the
        function should return 'assignment'. Otherwise, the search
        should continue. When the function 'inference' is called to run
        the AC-3 algorithm, the lists of legal values in 'assignment'
        should get reduced as AC-3 discovers illegal values.

        IMPORTANT: For every iteration of the for-loop in the
        pseudocode, you need to make a deep copy of 'assignment' into a
        new variable before changing it. Every iteration of the for-loop
        should have a clean slate and not see any traces of the old
        assignments and inferences that took place in previous
        iterations of the loop.
        """
        # TODO: IMPLEMENT THIS
        #pass
       
       
        # Insert all arcs both ways (for inference in AC3)
        # The first time this is running - all arcs (in outer csp) are present
        # THIS IS DONE IN backtrack_search()
        #queue = self.get_all_arcs()
        #print("\n\nSTARTING BACKTRACK() with assignment: ", assignment)
        print("\n\nSTARTING BACKTRACK()")
        
        # Keep the assignment untouched if needing to revert
        assignmentPreserved = copy.deepcopy(assignment)

        # FIRST CHECK IF SOLUTION EXIST = assignment is complete
  
        solution       = False   # Placeholder for the solution-test (is True when solution found)
        previousValues = []      # Keep track of already tested values (in this backtrack-loop)
        counterAC3     = 0       # Count number of AC-3 runs
        
        solutionTest = [x for x in assignment if (len(assignment[x]) == 1)]
        print("\n\nSolutionTest is: ", solutionTest)
        if (len(assignment) == len(solutionTest)):
                print("\n\n\nWE DO ACTUALLY REACH A SOLUTION???\n\n\n")
                print("Assignment: ", assignment)
                solution = True
        
        if (not solution):

            """
            # Alternative solution check
            solution = True
            for i in assignment:
                if (len(assignment[i]) != 1):
                    solution = False
            """   
        
            

            #constraintsCurrent = self.add_constraints_one_way(i, j, filter_function)

            # Next is to choose the candidate in current which rules out the least of options for next values
            # I.e. if two neighbours (not neighbours to each other) to current are "red" and "blue", 
            # leaving current to be "green", any neighbours of "red" will be assigned "blue" and vise versa (not "green")
            # The first loop will not do this as the first color in line (RGB) will be chosen (red)

            # Get the (best)candidate variable to assign a value to first (there are different methods)
            # Principle used here is 1) DEGREE-HEURISICS = if equal number of values per variable, 
            # choose the variable with the most neighbours. (For coloring and sudoku, we know that
            # there are equally number of values for all variables, in other cases this can be different,
            # and it can be better to start with LEAST-VALUES-HEURISTICS
            # Next use 2) LEAST-VALUES-HEURISTICS = start with the variable
            # with the least number of legal values (left).

            # Added this:
            queue = copy.deepcopy(self.get_all_arcs())  # Pair-vise list of (all) two-ways arcs (NO domain info)
            queuePreserved = copy.deepcopy(queue)
            #print("Queue is: ", queue)
            #print("Queue type is: ", type(queue))
            
            # Assign first available legal values to all nodes with no neighbours (single)
            singles = [x for x in assignment if (len(self.get_all_neighboring_arcs(x)) == 0)]
            if (len(singles) > 0):
                for s in singles:
                    assignment[s] = assignment[s][0:1]
          

            # HAVE AN OPTION FOR ROLLBACK OF "assignment" from here
            current = self.select_unassigned_variable(assignment)
  
            # Get all available values for current 
            # TODO: Check if assignment is complete - if so - return assignment

            # Prevent running if value-domain of current is 0 (domain is empty)
            # TODO: check if this is necessary
            if(current != ""):
                
                legalValues = [x for x in assignment[current] if (not (x in previousValues))]
                # Try all available values for current before calling next backtrack recursively
                while (len(previousValues) < len(self.domains[current])):
                    # ASSIGN A VALUE to CURRENT (= remove all but the selected value in assignment)
                    # Keep track of used values in previousValues[]
                    
                    # TODO: This is not working properly!!!
                   
                    print("Available values in currents domain: ", assignment[current]) 
                    item = legalValues.pop()
                    
                    previousValues.append([item])
                    assignment[current] = [item]
                    #previousValues.append(assignment[current][0:1])
                    #assignment[current] = assignment[current][0:1]
                    print("In first of these: ", assignment[current])

                    print("\n\tSTART EXPLORING (current): ", current)
 
                    # Taking out all edges in queue that has current first in its pair
                    currentNeighbours = [x for x in queue if current in x[0:1]]
                    print("Currents neighbours as pairs = ", currentNeighbours)
            
                    # Select sequential from the back of generated list)
                    neighbourpair = currentNeighbours.pop()
                    if (neighbourpair in queue):
                        queue.remove(neighbourpair)
                    # Get the Xj in neighbourpair position [1] for assigning
                    neighbour = neighbourpair[1]

                    # Assign a value to Xj (neighbour) that is allowed
                    # We do not keep track of these values, only for Xi (current)
                    for i in range(len(assignment[neighbour])):
                        if (assignment[current][0:1] == assignment[neighbour][i:i+1]):
                            continue
                        else:
                            assignment[neighbour] = assignment[neighbour][i:i+1]
                            break
                 
                    # RUN INFERENCE AC-3 with newly assigned values
                    # IF THIS IS RUN ONLY ONCE - only the first value is tried!!!!
                    counterAC3 += 1
                    print("#AC-3 is called: ", counterAC3)
                    inferenceExecuted = self.inference(assignment, queue)
   
                    """
                    print("Queue is: ", queue)
                    print("Type queue is: ", type(queue))
                    for qu in queue:
                        print("qu is: ", qu)
                        for que in qu:
                            print("que is: ", que)
                    """

                    if (inferenceExecuted):
                        # Get result (pseudo code)
                        a,s = self.backtrack(assignment)

                        if (s):
                            # Return result (pseudo code)
                            return a, s

                    # FAILURE (pseudo code): revert to previous state
                    elif (not inferenceExecuted):
                        print("RESET assignment due to AC-3 failure")
                        print("Values already tried are: ", previousValues)
                        print("Current domains before reset: ", assignment[current])
                        print("Length of previousValues is: ", len(previousValues))
                        print("Length of currents domain is: ", len(self.domains[current]))
                        assignment    = copy.deepcopy(assignmentPreserved)
                        print("Length of currents domain in reset assignment is: ", len(assignment[current]))
                        print("Currents domain after reset",assignment[current]) 
                        # Reset all legal values in currents domain
                        
                     
                        #queue = queuePreserved
                        #self.backtrack(assignment)

        # If solution = TRUES
        print("\n\n\nTHIS IS THE END OF BACKTRACK\n\n\n")
        return assignment, solution

    
    # Changed, added the queue at the only possible tracker of unasigned pairs and variables
    def select_unassigned_variable(self, assignment):
        degree    = 0      # Placeholder for degree
        candidateV = ""    # Placeholder for candidate (least remaining values) to be explored next
        candidateN = ""    # Placeholder for candidate (most neighbours) to be explored next
        oneLeft   = False  # Placeholder for bool indicating if one neighbour has only one value left
        value     = ""     # Placeholder for the oneLeft-value
        counter   = 0      # Counting number of AC-3 loops
        
        """The function 'Select-Unassigned-Variable' from the pseudocode
        in the textbook. Should return the name of one of the variables
        in 'assignment' that have not yet been decided, i.e. whose list
        of legal values has a length greater than one.
        """
        # TODO: IMPLEMENT THIS
    
        # In both coloring and sudoku, every field can be covered by every "value" as a defalult
        # That is, the MINIMUM-REMAINING-VALUES strategy gives nothing in the first round as 
        # all candidates have the same number of possible values
        
        # That implies that a degree heuristic (Russel and Norvig 2020, p.193) will be a
        # good candidate for SELECT-UNASSIGNED-VARIABLE in its FIRST iteration.
        # For a general purpose solver, the MINIMUM-REMAINING-VALUES strategy should
        # be used before DEGREE (number of neighbours) 
    
        # Find the variable with highest degree heuristics and explore it first
        
        #print("Element in assignment (inside select_unassigned_variable) is: ", assignment)  
        #print("Assignment is of type: ", type(assignment))
        
        # Find the right ORDER-DOMAIN-VALUES
        # Pick the first value and assign it to current 
        # (by removing the other choices from currents domain) 
        #assignment.pop(current)

        # if possible, do not choose those values for the upcomming "current"
        # if current is a neighbour.
        # HERE: assign the value in the first positon
        #assignment[current] = self.domains[current][0:1]
        
        
        # Find the right ORDER-DOMAIN-VALUES
        # Find variables with only ONE value left 
        findOneRemainingValue = [x for x in assignment if (len(self.domains[x]) == 1)]
        print("Find one remaining looks like this: ", findOneRemainingValue)

        leastNumberValues    =  99  # If 0 ---> failure
        mostNumberNeighbours = -99  # Can be 0
      
        # Just assign as they appear in list
        for c in assignment:            # c is the key and candidate (SA, WA..)
            
            numN = len(self.get_all_neighboring_arcs(c)) # number of neighbours
            
            if (numN == 0):
                print("SKIP variable without any neighbours: ", c)
                continue
                
            numV = len(assignment[c])  # number of values
            print("C is ", c)
            print("Num Values = ", numV)
            print("Num Neighbours = ", numN)

            if (leastNumberValues > numV):
                leastNumberValues = numV
                candidateV = c
                print("New low vaue candidate is: ", c)

            if (mostNumberNeighbours < numN):
                mostNumberNeighbours = numN
                candidateN = c
                print("New high neighbour candidate is: ", c)
       
        # Change the value domain to the first in the initial domain
        #self.domains[candidate] = self.domains[candidate][0:1] 
        print("Candidate with most neighbours is: ", candidateN)
        print("Number of neighbours is: ", mostNumberNeighbours)
        print("Candidate with least number of domain values is: ", candidateV)
        print("Number of domain values: ", leastNumberValues)
        print("Candidatate type is: ", type(candidateN))
        
        # FAIL FIRST
        if (leastNumberValues < 3 and mostNumberNeighbours < 3):
            print("Returning FAIL FIRST = ", candidateV)
            return candidateV
        # MOST NEIGHBOURS
        else:
            print("Returning MOST NEIGHBOURS = ", candidateN)
            return candidateN
            
        
    # This function is changed - added "first" to send the variable 
    # chosen by "select_unassigned_variable()"     
    def inference(self, assignment, queue):
        """The function 'AC-3' from the pseudocode in the textbook.
        'assignment' is the current partial assignment, that contains
        the lists of legal values for each undecided variable. 'queue'
        is the initial queue of arcs that should be visited.
        """
        # TODO: IMPLEMENT THIS
        #pass
        
        # TODO check if this is necessary (probably not)
        #cqueue = copy.deepcopy(queue)
        
        # First time this is run:
        # The queue is all arcs and the assignment is empty {}
        
        
        while (len(queue) > 0):
            # Pops the que = take the item at the back of the list
            # TODO: Check if it matters where we start - the assigned values
            # on ONE pair is set in the incoming assignment
            # it is no consideration of which to assign next here!
            pair = queue.pop()  # By this the pair investigated is removed
            Xi = pair[0]
            Xj = pair[1]
            
            print("Parent Xi in AC3 is: ", Xi)
            print("Neighbour in AC3 is: ", Xj)

            revised = self.revise(assignment, Xi, Xj)

            if (revised):
                print("IS REVISED variable: ", Xi)
                print("against neighbour: ", Xj)
                
                if (len(assignment[Xi]) == 0):
                    # Domain is empty (no legal values left in domain for Xi)
                    print("\n\nAC-3 FOUND EMPTY DOMAIN returns FALSE\n\n")
                    return False
 
                # NB! The neighbourlist is ONE_WAY with Xi at position [0]
                #neighbours = self.get_all_neighboring_arcs(Xi)
        
                # Taking out all edges in queue that has current first in its pair
                neighbours = [x for x in self.get_all_neighboring_arcs(Xi)]
                print("ADDED neighbours (Xk,Xi) except (Xj,Xi) back to queue: ", neighbours)
                neighbours.remove((Xj,Xi))
                
                # Merge two lists without duplicates:
                queue = list(set(queue+neighbours))            
                    
                print("New domain for revised variable is: ", assignment[Xi])
                print("Length of queue is: ", len(queue))

            else:
                print("NOT REVISED variable: ", Xi)
                print("against neighbour: ", Xj)
                
            print("Deleted from Queue item: ", pair)    
            print("\n\nAC-3 returns TRUE\n\n")
            #queue = cqueue
        return True
    

    def revise(self, assignment, Xi, Xj):
        """The function 'Revise' from the pseudocode in the textbook.
        'assignment' is the current partial assignment, that contains
        the lists of legal values for each undecided variable. 'i' and
        'j' specifies the arc that should be visited. If a value is
        found in variable i's domain that doesn't satisfy the constraint
        between i and j, the value should be deleted from i's list of
        legal values in 'assignment'.
        """
        # TODO: IMPLEMENT THIS
        #pass

        revised = False
        
        print("Incoming Xi domains: ", assignment[Xi])
        print("Incoming Xj domains: ", assignment[Xj])
        #print("Assignment is: ", assignment)

        for d in assignment[Xi]:
            # Test if directly passing as OK
            if (not(d in assignment[Xj])):
                continue
                
            if (d in assignment[Xj]):
                if (len(assignment[Xj]) == 1):
                    assignment[Xi].remove(d)  # This can result in an empty domain for Xi
                    print("VALUE REMOVED from Xi domain was: ", d)
                    revised = True
            
                elif(len(assignment[Xj]) > 1):
                    # OK
                    print("AVAILABLE VALUE MATCHES for Xi")
                     
        return revised


def create_map_coloring_csp():
    """Instantiate a CSP representing the map coloring problem from the
    textbook. This can be useful for testing your CSP solver as you
    develop your code.
    """
    csp = CSP()
    states = ['WA', 'NT', 'Q', 'NSW', 'V', 'SA', 'T']
    edges = {'SA': ['WA', 'NT', 'Q', 'NSW', 'V'], 'NT': ['WA', 'Q'], 'NSW': ['Q', 'V']}
    colors = ['red', 'green', 'blue']
    for state in states:
        csp.add_variable(state, colors)
    for state, other_states in edges.items():
        for other_state in other_states:
            csp.add_constraint_one_way(state, other_state, lambda i, j: i != j)
            csp.add_constraint_one_way(other_state, state, lambda i, j: i != j)
    return csp


def create_sudoku_csp(filename):
    """Instantiate a CSP representing the Sudoku board found in the text
    file named 'filename' in the current directory.
    """
    csp = CSP()
    board = list(map(lambda x: x.strip(), open(filename, 'r')))

    for row in range(9):
        for col in range(9):
            if board[row][col] == '0':
                csp.add_variable('%d-%d' % (row, col), list(map(str, range(1, 10))))
            else:
                csp.add_variable('%d-%d' % (row, col), [board[row][col]])

    for row in range(9):
        csp.add_all_different_constraint(['%d-%d' % (row, col) for col in range(9)])
    for col in range(9):
        csp.add_all_different_constraint(['%d-%d' % (row, col) for row in range(9)])
    for box_row in range(3):
        for box_col in range(3):
            cells = []
            for row in range(box_row * 3, (box_row + 1) * 3):
                for col in range(box_col * 3, (box_col + 1) * 3):
                    cells.append('%d-%d' % (row, col))
            csp.add_all_different_constraint(cells)

    return csp


def print_sudoku_solution(solution):
    """Convert the representation of a Sudoku solution as returned from
    the method CSP.backtracking_search(), into a human readable
    representation.
   
    for row in range(9):
        for col in range(9):
            print(solution['%d-%d' % (row, col)][0]),
            if col == 2 or col == 5:
                print('|'),
        print("")
        if row == 2 or row == 5:
            print('------+-------+------')
    """     
            
    for row in range(9):
        for col in range(9):
            print(solution['%d-%d' % (row, col)][0], ' ', end=''),
            if col == 2 or col == 5:
                print('|', ' ', end=''),
        print("")
        if row == 2 or row == 5:
            print('---------+-----------+---------')

